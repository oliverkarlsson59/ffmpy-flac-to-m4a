import ffmpy
import os
filepath = "../../../Demetori_m4a"
destination = "../../../Demetori_m4a/m4a/"

for filename in os.listdir(filepath):
    f = os.path.join(filepath, filename)
    if os.path.isfile(f):
        output = filename[:-5]
        ff = ffmpy.FFmpeg(
            inputs={f: "-r 25"},
            outputs={destination + output + '.m4a' : "-vcodec copy"},
        )
        ff.run()
    else:
        break


